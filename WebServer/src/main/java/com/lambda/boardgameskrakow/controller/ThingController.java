package com.lambda.boardgameskrakow.controller;

import com.lambda.boardgameskrakow.exception.ResourceNotFoundException;
import com.lambda.boardgameskrakow.model.dto.ThingInputDto;
import com.lambda.boardgameskrakow.model.dto.ThingOutputDto;
import com.lambda.boardgameskrakow.model.entity.Thing;
import com.lambda.boardgameskrakow.service.ThingService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/things")
@CrossOrigin(origins = "*", maxAge = 3600)
public class ThingController {

    private final ThingService thingService;
    private ModelMapper modelMapper;

    @Autowired
    public ThingController(ThingService thingService) {
        this(thingService, new ModelMapper());
    }

    public ThingController(ThingService thingService, ModelMapper modelMapper) {
        this.thingService = thingService;
        this.modelMapper = modelMapper;
    }

    @GetMapping("{id}")
//    @PreAuthorize("hasAnyRole('USER', 'ADMIN')")
    public ThingOutputDto getThingById(@PathVariable(value = "id") int id) throws ResourceNotFoundException {
        if (!thingService.findThingById(id)) {
            throw new ResourceNotFoundException("Thing", "id", id);
        }

//        SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        Thing thing = thingService.getThingById(id);
        ThingOutputDto thingOutputDto = modelMapper.map(thing, ThingOutputDto.class);

        return thingOutputDto;
    }

    @GetMapping()
//    @PreAuthorize("hasAnyRole('USER', 'ADMIN')")
    public List<ThingOutputDto> getAllThings() {

//        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
//        String username = ((UserDetails) principal).getUsername();

        List<Thing> things = thingService.findAll();
        List<ThingOutputDto> planOutputsDto = new ArrayList<>();

        things.forEach(x -> {
                    ThingOutputDto thingOutputDto = modelMapper.map(x, ThingOutputDto.class);
                    planOutputsDto.add(thingOutputDto);
                }
        );

        return planOutputsDto;
    }

    @PostMapping()
//    @PreAuthorize("hasAnyRole('USER', 'ADMIN')")
    public ThingOutputDto createThing(@RequestBody ThingInputDto thingInputDto) {

//        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
//        String username = ((UserDetails) principal).getUsername();

//        try {
//            return thingService.generateOptimalThing(thingInputDto, username);
//        } catch (InvalidDataProvidedException e) {
//            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
//        }

        return modelMapper.map(thingService.saveThing(modelMapper.map(thingInputDto, Thing.class)), ThingOutputDto.class);
    }
}