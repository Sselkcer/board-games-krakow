package com.lambda.boardgameskrakow.service.implementation;

import com.lambda.boardgameskrakow.model.entity.Thing;
import com.lambda.boardgameskrakow.repository.ThingRepository;
import com.lambda.boardgameskrakow.repository.UserRepository;
import com.lambda.boardgameskrakow.service.ThingService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ThingServiceImpl implements ThingService {

    private final ThingRepository thingRepository;
    private final UserRepository userRepository;
    private ModelMapper modelMapper;

    @Autowired
    public ThingServiceImpl(ThingRepository thingRepository, UserRepository userRepository) {
        this(thingRepository, userRepository, new ModelMapper());
    }

    public ThingServiceImpl(
            ThingRepository thingRepository,
            UserRepository userRepository,
            ModelMapper modelMapper
    ) {
        this.thingRepository = thingRepository;
        this.userRepository = userRepository;
        this.modelMapper = modelMapper;
    }

    public boolean findThingById(int id) {
        return thingRepository.existsById(id);
    }

    public Thing getThingById(int id) {
        return thingRepository.getOne(id);
    }

    @Override
    public Thing saveThing(Thing thing) {

//        OptimizedActivity optimizedActivity = thing.getOptimizedActivity();
//        optimizedActivity.setThing(thing);
//        thing.setOptimizedActivity(optimizedActivity);
//
//        List<Lesson> lessons = thing.getLessons();
//        lessons.forEach(x -> {
//                    x.setThing(thing);
//
//                    Term term = x.getTerm();
//                    term.setLesson(x);
//                    x.setTerm(term);
//                }
//        );
//        thing.setLessons(lessons);

        return thingRepository.save(thing);
    }

    @Override
    public List<Thing> findAll() {
        return thingRepository.findAll();
    }

    @Override
    public void delete(int id) {

    }

    @Override
    public Thing findByThingname(String thingname) {
        return null;
    }

    @Override
    public Thing findById(int id) {
        return null;
    }


}