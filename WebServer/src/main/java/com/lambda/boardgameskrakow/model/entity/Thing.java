package com.lambda.boardgameskrakow.model.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Set;

@Getter
@Setter
@Entity
@Table(name = "thing")
public class Thing {

    @Id
    @Column(name = "thing_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "thing_name", length = 100)
    private String name;

    @Column(name = "thing_description", length = 100)
    private String description;

    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinTable(name = "user_thing", joinColumns = {
            @JoinColumn(name = "user_id")}, inverseJoinColumns = {
            @JoinColumn(name = "thing_id")})
    private Set<User> users;

    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinTable(name = "type_thing", joinColumns = {
            @JoinColumn(name = "type_id")}, inverseJoinColumns = {
            @JoinColumn(name = "thing_id")})
    private Set<Type> types;
}
