package com.lambda.boardgameskrakow.model.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ThingOutputDto {

    private int id;

    private String name;

    private String description;
}
